import 'package:flutter/material.dart';

class ComponentsBookList extends StatelessWidget {
  const ComponentsBookList(
      {super.key,
      required this.imgUrl,
      required this.goodsTitle,
      required this.textBody,
      required this.callback});

  final String imgUrl;
  final String goodsTitle;
  final String textBody;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              child: Image.asset(
                imgUrl,
                width: 500,
                height: 250,
                fit: BoxFit.contain,
              ),
            ),
            Container(
              child: Text(goodsTitle),
            ),
            Container(
              child: Text(textBody),
            ),
          ],
        ),
      ),
    );
  }
}
