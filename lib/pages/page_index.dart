import 'package:book1_bombom_app/compornents/components_book_list.dart';
import 'package:book1_bombom_app/pages/page_view_details.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  int _currentPage = 0;
  final CarouselController _pageController = CarouselController();
  List imageList = ['assets/book1.jpg', 'assets/book2.jpg', 'assets/book3.jpg'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        centerTitle: true,
        title: Text(
          'BomBom',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        elevation: 0.0,
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.search),
          ),
        ],
      ),
      drawer: Drawer(),
      body: Padding(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: Stack(
                children: [
                  PageView(
                    onPageChanged: (int page) {
                      setState(() {
                        _currentPage = page;
                      });
                    },
                    children: [
                      Image.asset('assets/book1.jpg'),
                      Image.asset('assets/book2.jpg'),
                      Image.asset('assets/book3.jpg'),
                    ],
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          for (int i = 0; i < 3; i++)
                            Container(
                              margin: EdgeInsets.all(4.0),
                              width: 12.0,
                              height: 12.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: _currentPage == i
                                    ? Colors.grey
                                    : Colors.grey.withOpacity(0.5),
                              ),
                            ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              child: GridView(
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 5 / 6,
                  mainAxisSpacing: 100,
                  crossAxisSpacing: 100,
                ),
                children: [
                  ComponentsBookList(
                    imgUrl: 'assets/book1.jpg',
                    goodsTitle: '철학은 날씨를 바꾼다',
                    textBody: '내용이다',
                    callback: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => PageViewDetails()));
                    },
                  ),
                  ComponentsBookList(
                      imgUrl: 'assets/book2.jpg',
                      goodsTitle: '호박눈의 산토끼',
                      textBody: '이ㅏㅓㄻ',
                      callback: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => PageViewDetails()));
                      }),
                  ComponentsBookList(
                      imgUrl: 'assets/book3.jpg',
                      goodsTitle: '지긋지긋한 사람을 죽이지 않고 없애는 법',
                      textBody: '이ㅏㅓㄻ',
                      callback: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => PageViewDetails()));
                      }),
                  ComponentsBookList(
                      imgUrl: 'assets/book3.jpg',
                      goodsTitle: '지긋지긋한 사람을 죽이지 않고 없애는 법',
                      textBody: '이ㅏㅓㄻ',
                      callback: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => PageViewDetails()));
                      }),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildBanner(String text, int index) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0), // 원하는 둥근 정도를 설정합니다.
        color: Colors.blueGrey,
      ),
      child: Center(child: Text(text)),
    );
  }
}
